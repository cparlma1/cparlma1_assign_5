#pragma once
#include <opencv2\imgproc\imgproc.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <vector>
#include <iostream>
#include <vector>
#include <algorithm>
#include <map>
#include <stdlib.h>     /* srand, rand */


using namespace std;
using namespace cv;


class processor
{
public:
	processor();
	processor(string image_name);
	~processor();

	void quick_display();

	void quick_display(Mat im);

	int sub_index(int y, int x);

	void classify();

	float average(Mat image);


	int y_x_to_sub(int y, int x);

	void writeImage(string name, vector<int> classes);

	void writeImageN1(vector<int> clas);

	void writeImageN2(string name, vector<int> classes);

	void calc_sub_avgs();

	void writeImageN3(string name, vector<int> classes);

	void writeImage(vector<int> classes);

	void writeImage();

	void nearestNeighbor();

	void resultsPrinter();

	void mykmeans(int k);

	int cluster(int sample, int cluster1, int cluster2, int cluster3);

	void mykmeans();

	int closest_average(float cavg, vector<int> clust);

	void means();

	void writeImageT(vector<int> clas);

	void writeImageK1();


	void nearestNeighbor(Mat image);


	//IMAGES AND SUB IMAGES 
	Mat image;
	Mat tmpImage;
	vector<Mat> sub_images;
	vector<int> classes;
	vector<int> test_classes;
	map<float, int> avg_index_map;
	vector<int> sub_images_avgs;
	vector<int> kmeans_classes;
	vector<int> cluster_centers;


};
