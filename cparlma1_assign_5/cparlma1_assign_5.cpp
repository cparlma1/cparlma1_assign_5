// cparlma1_assign_5.cpp : Defines the entry point for the console application.
//
#include <string>

#include "stdafx.h"
#include "processor.h"



int main()
{
	/*HARD CODED FILE NAMES */
	string playframe1 = "PlayFrame1_320x200.bmp";
	string playframe2 = "PlayFrame2_320x200.bmp";
	string tennis1 = "TennisFrame1_320x200.bmp";
	string tennis2 = "TennisFrame2_320x200.bmp";
	string pig_e = "TestImage-even-width.bmp";
	string pig_o = "TestImage-odd-width.bmp";

	//load pig_e image  into a processor  object 
	processor pig_processor(pig_e);
	pig_processor.quick_display(pig_processor.image);
	pig_processor.nearestNeighbor();

	pig_processor.resultsPrinter();
	waitKey(0);
	pig_processor.writeImage("M1.bmp", pig_processor.classes);
	pig_processor.writeImageN1(pig_processor.test_classes);
	pig_processor.writeImageN2("N2.bmp",pig_processor.classes);
	pig_processor.writeImageN3("N3.bmp",pig_processor.classes);
	pig_processor.writeImageT(pig_processor.classes);

	pig_processor.mykmeans();
	waitKey(0);
	pig_processor.writeImageK1();
	waitKey(0);

    return 0;
}


