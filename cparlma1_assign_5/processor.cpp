#include "stdafx.h"
#include "processor.h"
#include <time.h>
#include <numeric>


processor::~processor()
{
}

processor::processor()
{
}


//processor CONSTRUCTOR  takes string name 
//read a image 
//fill in sub image vector 
processor::processor(string image_name)
{	//read image 
	image = imread(image_name, IMREAD_GRAYSCALE);
	//seed random 
	srand(time(NULL));

	//create sub images 
	int width = image.cols/4;
	int height = image.rows/4;
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
			Rect r(i*width, j*height, width, height);
			sub_images.push_back(Mat(image, r).clone());
		}
	}

	for (int i = 0; i < 16; i++) {
		test_classes.push_back(0);
		avg_index_map[average(sub_images[i])] = i;

	}
	calc_sub_avgs();
	//classify the sub images 
	classify();
}




//QUICK DISPLAY for showing the main image 
//change for individuals, overload... 
	//can c++ overload ???? 
void processor::quick_display(Mat im) {
	string display_name = "dis";
	namedWindow(display_name);
	imshow(display_name, im);
	waitKey(0);
}




//return the index of an image in sub_images based on (y,x) values 
int processor::sub_index(int y, int x) {
	return 4 * x + y;
}



/*CLASSIFY
iterate through sub imagages calculating average grey intensity 
give the classification 
		class 1: 0 <= G < 125
		class 2: 125 <= G < 275
		class 3: 175 <= G <= 255
*/
void processor::classify() {
	for (int i = 0; i < 16; i++) {
		int avg = average(sub_images[i]);
		if		(avg >= 175)	{classes.push_back(1);}
		else if (avg >= 125)	{classes.push_back(2);}
		else					{classes.push_back(3);}
	}
}





/*AVERAGE 
get the average greyscale value of an image 
*/
float processor::average(Mat image ) {
	int area = image.cols * image.rows;
	int sum = 0;
	for (int y = 0; y < image.rows; y++) {
		for (int x = 0; x < image.cols; x++) {
			sum += image.at<uchar>(y, x);
		}
	}
	return  (float)sum / (float)area;
}






//lazy hard coded values for finding subimage index of global x, y values 
int processor::y_x_to_sub(int y, int x) {
	int subx = 1;
	int suby = 1;
	//get subindex x
	if (x >= 240)		{ subx = 3; }
	else if (x >= 160)	{ subx = 2; }
	else if (x >= 80)	{ subx = 1; }
	else				{ subx = 0; }
	//sub index y
	if (y >= 180)	{ suby = 3; }
	else if (y >= 120)	{ suby = 2; }
	else if (y >= 60)	{ suby = 1; }
	else { suby = 0; }

	return sub_index(suby, subx);

}





//find the nearest cluster 
int processor::cluster(int sample, int c1, int c2, int c3) {
	int sample_average = average(sub_images[sample]);
	int difference[] = {
			abs(average(sub_images[c1]) - sample_average),
			abs(average(sub_images[c2]) - sample_average),
			abs(average(sub_images[c3]) - sample_average),
	};
	
	//find the min index 
	int min = 0;
	for (int i = 0; i < 3; i++) {
		if(difference[i] < difference[min]){
			min = i;
		}
	}
	return min;
}






void processor::mykmeans() {
	printf("mykmeans()\n");
	//1 choose unique random cluster centers 
	int ccenter1 = rand() % 15;
	int ccenter2 = rand() % 15;
	int ccenter3 = rand() % 15;
	while (ccenter1 == ccenter2) { ccenter2 = rand() % 15; }
	while (ccenter3 == ccenter1 || ccenter3 == ccenter2) { ccenter3 = rand() % 15; };
	printf("%d, %d, %d\n", ccenter1, ccenter2, ccenter3);

	//get averages of sub_images  
	vector<int> averages;
	for(int i = 0; i < 16; i++){
		averages.push_back(average(sub_images[i]));
	}

	int ccprev1 = ccenter1;
	int ccprev2 = ccenter2;
	int ccprev3 = ccenter3;
		vector<int> cluster1;
		vector<int> cluster2;
		vector<int> cluster3;


		/*2 distribute the pattern samples into the cluster domain
		according to the distance from the sample to the cluster center
		*/
	do {
		cluster1.clear();
		cluster2.clear();
		cluster3.clear();
		cluster_centers.clear();
		kmeans_classes.clear();
		for (int i = 0; i < 16; i++) {
			int clust = cluster(i, ccenter1, ccenter2, ccenter3);
			printf("i:%d,  clust:%d\n", i, clust);
			if (clust == 0) { cluster1.push_back(i); }
			if (clust == 1) { cluster2.push_back(i); }
			if (clust == 2) { cluster3.push_back(i); }
			kmeans_classes.push_back(clust);
		}
		/*3 update cluster centers */

		float c1avg = accumulate(cluster1.begin(), cluster1.end(), 0.0 / cluster1.size());
		float c2avg = accumulate(cluster2.begin(), cluster2.end(), 0.0 / cluster2.size());
		float c3avg = accumulate(cluster3.begin(), cluster3.end(), 0.0 / cluster3.size());
		if (cluster1.size() == 0) { c1avg = 0; }
		if (cluster2.size() == 0) { c3avg = 0; }
		if (cluster3.size() == 0) { c2avg = 0; }
		printf("c1avg(%f), c2avg(%f),  c3avg(%f)\n", c1avg, c2avg, c3avg);

		ccprev1 = ccenter1;
		ccprev2 = ccenter2;
		ccprev3 = ccenter3;
		ccenter1 = closest_average(c1avg, cluster1);
		ccenter2 = closest_average(c1avg, cluster2);
		ccenter3 = closest_average(c1avg, cluster3);
		printf("prev1(%d), newc1(%d)\n", ccprev1, ccenter1);
		printf("prev2(%d), newc2(%d)\n", ccprev2, ccenter2);
		printf("prev3(%d), newc3(%d)\n", ccprev3, ccenter3);
	} while (ccprev1 != ccenter1 || ccprev2 != ccenter2 || ccprev3 != ccenter3);
	//store cluster center indexs 
	cluster_centers.push_back(ccenter1);
	cluster_centers.push_back(ccenter2);
	cluster_centers.push_back(ccenter3);
	sort(cluster_centers.begin(), cluster_centers.end(), [](int a, int b) {return a < b; });

}





//find the neares neighbor and classify the image 
void processor::nearestNeighbor() {
	//iterate through bottom half of the image 
	int nn[] = { 0,0,0,0 };
	map<int, float> dist_index;	//create a map to remember float to vector index 
	map<float, int> dist_index2;
	vector<float> distances;	//vector of distances 

	//assign a class to every vector element corresponding to all sub images
	for (int y = 0; y < 4; y++) {
		for (int x = 0; x < 4; x++) {
			nn[0] = nn[1] = nn[2] = nn[3] = 0;
			dist_index.clear();
			dist_index2.clear();
			distances.clear();

			//iterate through top half and calulate distances 
			//top half has training classes assigned. 
			//this is currently using average?. 
			// switch to distance from, class average of a known block class ? 
			//(125/2), ((125+175)/2), ((175+255)/2)
			for (int j = 0; j < 2; j++) {
				for (int i = 0; i < 4; i++) {
					float distance = abs((float)(average(sub_images[sub_index(y, x)])
						- average(sub_images[sub_index(j, i)])));
					distances.push_back(distance);
					dist_index[sub_index(j, i)] = distance;
					dist_index2[distance] = sub_index(j, i);
					//printf("map(%d) = %f\n", sub_index(j,i), dist_index.find(sub_index(j,i))->second);
					//printf("map(%f) = %d\n", distance ,dist_index2.find(distance)->second);
				}
			}
			//sort distances 
			sort(distances.begin(), distances.end(), [](int a, int b) {return a < b; });

			//find the nearest neighbor using the map to recall index 
			//find the smallest values representing the nearest neighbor 
			for (int i = 0; i < 3; i++) {
				float distance = distances[i];
				int index_match = dist_index2.find(distance)->second;
				int classs = classes[index_match];
				nn[classs]++;
				//	printf("dist(%f), match_index(%d), match_class(%d)\n", distance, index_match, classs);
			}
			//printf("(%d,%d) nn : 1:%d, 2:%d, 3:%d\n", y,x, nn[1], nn[2], nn[3]);

			//2 out of 3 is nearest neighbor 
			//find the nearest neighbor  from the closest three 
			for (int i = 1; i < 4; i++) {
				if (nn[i] >= 2) {
					test_classes[sub_index(y, x)] = i;
				}
			}
		}
	}
}


int processor::closest_average(float cavg, vector<int> clust) {
	vector<float> diff;
	for (int i = 0; i < clust.size(); i++) {
		diff.push_back(abs(cavg - average(sub_images[clust[i]])));
	}
	//min
	int min_index = 0;
	float min_diff = diff[0];
	for (int i = 0; i < clust.size(); i++) {
		if (diff[i] < diff[min_index]) {
			min_index = i;
		}
	}
	return clust[min_index];
}



/*----IMAGE WRITERS... LOTS --------**/ 

void processor::writeImage(string name, vector<int> classes) {
	printf("Write image M1 \n");
	int labels[] = { 0,128,255 };
	Mat imOut = image.clone();
	int width = image.cols / 4;
	int height = image.rows / 4;
	int subx = -1;
	int suby = -1;
	printf("height: %d, width %d\n", height, width);
	for (int y = 0; y < imOut.rows / 2; y++) {
		if (y%height == 0) { suby++; /*printf("y mod height = %d , suby:%d\n", y%height, suby); waitKey(0);*/ }
		subx = -1;
		for (int x = 0; x < imOut.cols; x++) {
			if (x%width == 0) { subx++; /*printf("x:%d\n", subx); */ }
			if (1 == classes[y_x_to_sub(y, x)]) {
				imOut.at<uchar>(y, x) = labels[2];
			}
			else if (2 == classes[y_x_to_sub(y, x)]) {
				imOut.at<uchar>(y, x) = labels[1];
			}
			else if (3 == classes[y_x_to_sub(y, x)]) {
				imOut.at<uchar>(y, x) = labels[0];
			}
			else {
				// printf("class of x, y (%d) not 1, 2, or  3 \n", classes[y_x_to_sub(y,x)]);
			}
		}
	}
	quick_display(imOut);
	waitKey(0);
	imwrite(name, imOut);
}



void processor::writeImageN1(vector<int> clas) {
	printf("Write image N1 \n");
	int labels[] = { 0,128,255 };
	Mat imOut = image.clone();
	int width = image.cols / 4;
	int height = image.rows / 4;
	int subx = -1;
	int suby = -1;
	/*	printf("height: %d, width %d\n", height, width);*/
	for (int y = 0; y < imOut.rows; y++) {
		if (y%height == 0) { suby++; /*printf("y mod height = %d , suby:%d\n", y%height, suby); waitKey(0);*/ }
		subx = -1;
		for (int x = 0; x < imOut.cols; x++) {
			if (x%width == 0) { subx++; /*printf("x:%d\n", subx);*/ }

			if (suby < 2) {
				if (1 == classes[y_x_to_sub(y, x)]) {
					imOut.at<uchar>(y, x) = labels[2];
				}
				else if (2 == classes[y_x_to_sub(y, x)]) {
					imOut.at<uchar>(y, x) = labels[1];
				}
				else if (3 == classes[y_x_to_sub(y, x)]) {
					imOut.at<uchar>(y, x) = labels[0];
				}
				else {
					// printf("class of x, y (%d) not 1, 2, or  3 \n", classes[y_x_to_sub(y,x)]);
				}
			}
			else {
				if (1 == clas[y_x_to_sub(y, x)]) {
					imOut.at<uchar>(y, x) = labels[2];
				}
				else if (2 == clas[y_x_to_sub(y, x)]) {
					imOut.at<uchar>(y, x) = labels[1];
				}
				else if (3 == clas[y_x_to_sub(y, x)]) {
					imOut.at<uchar>(y, x) = labels[0];
				}
			}
		}
	}
	quick_display(imOut);
	waitKey(0);
	imwrite("N1.bmp", imOut);

}




void processor::writeImageN2(string name, vector<int> classes) {
	printf("Write image N2 \n");
	int labels[] = { 0,128,255 };
	Mat imOut = image.clone();
	int width = image.cols / 4;
	int height = image.rows / 4;
	int subx = -1;
	int suby = 1;
	//printf("height: %d, width %d\n", height, width);
	for (int y = 0; y < imOut.rows; y++) {
		if (y%height == 0) { suby++; /*printf("y mod height = %d , suby:%d\n", y%height, suby); waitKey(0); */ }
		subx = -1;
		for (int x = 0; x < imOut.cols; x++) {
			if (x%width == 0) { subx++; /*printf("x:%d\n", subx); */ }
			if (1 == classes[y_x_to_sub(y, x)]) {
				imOut.at<uchar>(y, x) = labels[2];
			}
			else if (2 == classes[y_x_to_sub(y, x)]) {
				imOut.at<uchar>(y, x) = labels[1];
			}
			else if (3 == classes[y_x_to_sub(y, x)]) {
				imOut.at<uchar>(y, x) = labels[0];
			}
			else {
				// printf("class of x, y (%d) not 1, 2, or  3 \n", classes[y_x_to_sub(y,x)]);
			}
		}
	}
	quick_display(imOut);
	waitKey(0);
	imwrite(name, imOut);
}




void processor::calc_sub_avgs() {
	for (int i = 0; i < 16; i++) {
		sub_images_avgs.push_back(average(sub_images[i]));
	}
}



void processor::writeImageN3(string name, vector<int> classes) {
	printf("Write image N3 \n");
	int labels[] = { 0,128,255 };
	Mat imOut = image.clone();
	int width = image.cols / 4;
	int height = image.rows / 4;
	int subx = -1;
	int suby = 1;
	//printf("height: %d, width %d\n", height, width);
	for (int y = imOut.rows / 2; y < imOut.rows; y++) {
		if (y%height == 0) { suby++; /*printf("y mod height = %d , suby:%d\n", y%height, suby); waitKey(0);*/ }
		subx = -1;
		for (int x = 0; x < imOut.cols; x++) {
			if (x%width == 0) { subx++; /*printf("x:%d\n", subx);*/ }
			imOut.at<uchar>(y, x) = sub_images_avgs[sub_index(suby, subx)];
		}
	}
	quick_display(imOut);
	waitKey(0);
	imwrite(name, imOut);
}


void processor::resultsPrinter() {
	printf("\nNearest Neighbor Test \t(y,x) \n");
	printf("\tActual classes\t\tTest Results\n");
	for (int y = 0; y < 4; y++) {
		for (int x = 0; x < 4; x++) {
			printf("%d\t(%d,%d), = %d\t\t", sub_index(y, x), y, x, classes[sub_index(y, x)]);
			printf("(%d,%d), = %d\n", y, x, test_classes[sub_index(y, x)]);
		}
	}
}





void processor::writeImageT(vector<int> clas) {
	printf("Write image T \n");
	int labels[] = { 0,128,255 };
	Mat imOut = image.clone();
	int width = image.cols / 4;
	int height = image.rows / 4;
	int subx = -1;
	int suby = 1;
	printf("height: %d, width %d\n", height, width);
	for (int y = imOut.rows/2; y < imOut.rows; y++) {
		if (y%height == 0) { suby++; /*printf("y mod height = %d , suby:%d\n", y%height, suby); waitKey(0);*/ }
		subx = -1;
		for (int x = 0; x < imOut.cols; x++) {
			if (x%width == 0) { subx++; /*printf("x:%d\n", subx); */ }
			if (1 == classes[y_x_to_sub(y, x)]) {
				imOut.at<uchar>(y, x) = labels[2];
			}
			else if (2 == classes[y_x_to_sub(y, x)]) {
				imOut.at<uchar>(y, x) = labels[1];
			}
			else if (3 == classes[y_x_to_sub(y, x)]) {
				imOut.at<uchar>(y, x) = labels[0];
			}
			else {
				// printf("class of x, y (%d) not 1, 2, or  3 \n", classes[y_x_to_sub(y,x)]);
			}
		}
	}
	quick_display(imOut);
	waitKey(0);
	imwrite("T1.bmp", imOut);
}


void processor::writeImageK1() {
	printf("Write image K1\n");
	int labels[] = { 0,128,255 };
	Mat imOut = image.clone();
	int width = image.cols / 4;
	int height = image.rows / 4;
	int subx = -1;
	int suby = -1;

	for (int y = 0; y < imOut.rows; y++) {
		if (y%height == 0) { suby++; /*printf("y mod height = %d , suby:%d\n", y%height, suby); waitKey(0);*/ }
		subx = -1;
		for (int x = 0; x < imOut.cols; x++) {
			if (x%width == 0) { subx++; /*printf("x:%d\n", subx); */ }
			if (kmeans_classes[cluster_centers[0]] == kmeans_classes[y_x_to_sub(y, x)] ) {
				imOut.at<uchar>(y, x) = labels[2];
			}
			else if (kmeans_classes[cluster_centers[1]] == kmeans_classes[y_x_to_sub(y, x)]  ) {
				imOut.at<uchar>(y, x) = labels[1];
			}
			else if (kmeans_classes[cluster_centers[2]] == kmeans_classes[y_x_to_sub(y, x)] ) {
				imOut.at<uchar>(y, x) = labels[0];
			}
			else {
				 //printf("class of x, y (%d) not 1, 2, or  3 \n", classes[y_x_to_sub(y,x)]);
			}
		}
	}
	quick_display(imOut);
	waitKey(0);
	imwrite("K1.bmp", imOut);


}




//unused .. but interesting 
//https://docs.opencv.org/3.1.0/de/d63/kmeans_8cpp-example.html
void processor::means() {
	const int MAX_CLUSTERS = 5;
	Scalar colorTab[] =
	{
		Scalar(0, 0, 255),
		Scalar(0,255,0),
		Scalar(255,100,100),
		Scalar(255,0,255),
		Scalar(0,255,255)
	};
	Mat img(500, 500, CV_8UC3);
	RNG rng(12345);
	for (;;)
	{
		int k, clusterCount = rng.uniform(2, MAX_CLUSTERS + 1);
		int i, sampleCount = rng.uniform(1, 1001);
		Mat points(sampleCount, 1, CV_32FC2), labels;
		clusterCount = MIN(clusterCount, sampleCount);
		Mat centers;
		/* generate random sample from multigaussian distribution */
		for (k = 0; k < clusterCount; k++)
		{
			Point center;
			center.x = rng.uniform(0, img.cols);
			center.y = rng.uniform(0, img.rows);
			Mat pointChunk = points.rowRange(k*sampleCount / clusterCount,
				k == clusterCount - 1 ? sampleCount :
				(k + 1)*sampleCount / clusterCount);
			rng.fill(pointChunk, RNG::NORMAL, Scalar(center.x, center.y), Scalar(img.cols*0.05, img.rows*0.05));
		}
		randShuffle(points, 1, &rng);
		kmeans(points, clusterCount, labels,
			TermCriteria(TermCriteria::EPS + TermCriteria::COUNT, 10, 1.0),
			3, KMEANS_PP_CENTERS, centers);
		img = Scalar::all(0);
		for (i = 0; i < sampleCount; i++)
		{
			int clusterIdx = labels.at<int>(i);
			Point ipt = points.at<Point2f>(i);
			circle(img, ipt, 2, colorTab[clusterIdx], FILLED, LINE_AA);
		}
		imshow("clusters", img);
		char key = (char)waitKey();
		if (key == 27 || key == 'q' || key == 'Q') // 'ESC'
			break;
	}


}

